package kz.azatserzhanov.recyclerview.model


data class User(
    val name: String,
    val age: Int
): ParentInterface