package kz.azatserzhanov.recyclerview.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_user.view.*
import kz.azatserzhanov.recyclerview.R
import kz.azatserzhanov.recyclerview.model.ParentInterface
import kz.azatserzhanov.recyclerview.model.Second
import kz.azatserzhanov.recyclerview.model.User

class UserAdapter(
    private val textClickListener: (position: Int) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val data = mutableListOf<ParentInterface>()

    override fun getItemCount(): Int = data.size

    override fun getItemViewType(position: Int): Int =
        when (data[position]) {
            is User -> R.layout.item_user
            is Second -> R.layout.item_second
            else -> 0
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            R.layout.item_user -> UserViewHolder(inflater, parent)
            R.layout.item_second -> SecondViewHolder(inflater, parent)
            else -> throw IllegalArgumentException("View type not support")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is UserViewHolder -> holder.bind(data[position] as User, textClickListener)
            is SecondViewHolder -> holder.bind(data[position] as Second, textClickListener)
        }
    }

    fun addItems(list: List<ParentInterface>) {
        data.clear()
        data.addAll(list)
        notifyDataSetChanged()
    }

    fun addItem(user: ParentInterface) {
        data.add(user)
        notifyDataSetChanged()
    }

    fun removeItem(position: Int) {
        if (data.size > position) {
            data.removeAt(position)
            notifyDataSetChanged()
        }
    }

    private class UserViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.item_user, parent, false)) {
        private val userContainer = itemView.userContainer
        private val nameTextView = itemView.nameTextView
        private val ageTextView = itemView.ageTextView

        fun bind(user: User, textClickListener: (position: Int) -> Unit) {
            nameTextView.text = user.name
            ageTextView.text = user.age.toString()
            userContainer.setOnClickListener { textClickListener(adapterPosition) }
        }
    }

    private class SecondViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.item_second, parent, false)) {
        private val userContainer = itemView.userContainer
        private val nameTextView = itemView.nameTextView

        fun bind(second: Second, textClickListener: (position: Int) -> Unit) {
            nameTextView.text = second.name
            userContainer.setOnClickListener { textClickListener(adapterPosition) }
        }
    }
}