package kz.azatserzhanov.recyclerview

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import kz.azatserzhanov.recyclerview.model.Second
import kz.azatserzhanov.recyclerview.model.User
import kz.azatserzhanov.recyclerview.ui.UserAdapter

class MainActivity : AppCompatActivity() {

    private var userAdapter: UserAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        saveButton.setOnClickListener {
            if (nameEditText.text.isNotEmpty() && ageEditText.text.isNotEmpty()) {
                userAdapter?.addItem(
                    User(nameEditText.text.toString(), ageEditText.text.toString().toInt())
                )
            }
        }

        setupUserList()
    }

    private fun setupUserList() {
        userAdapter = UserAdapter(
            textClickListener = { position ->
                userAdapter?.removeItem(position)
            }
        )

        val selectPrayerManager = LinearLayoutManager(this)
        usersRecyclerView.apply {
            layoutManager = selectPrayerManager
            adapter = userAdapter
        }

        userAdapter?.addItems(
            listOf(
                User("Max", 29),
                User("Karina", 23),
                Second("Table"),
                Second("Chair")
            )
        )
    }
}
